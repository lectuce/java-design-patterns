package command;

public class User {
    public static void main(String[] args) {
        var remoteController = new RemoteController();
        var light = new Light();
        var lightOnCommand = new LightOnCommand(light);
        var lightOffCommand = new LightOffCommand(light);

        remoteController.setCommand(lightOnCommand);
        remoteController.setCommand(lightOffCommand);

        new User().swap(1 ,2);
    }

    private void swap(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("After swap : " + a + " : " + b);
    }
}
