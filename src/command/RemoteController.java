package command;

public class RemoteController {
    public void setCommand(Command command) {
        command.execute();
    }
}
