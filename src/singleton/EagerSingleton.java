package singleton;

public class EagerSingleton {
    private static final EagerSingleton INSTANCE = new EagerSingleton();

    private EagerSingleton() {
        System.out.println("Eager Singleton");
    }

    public static EagerSingleton getInstance() {
        return INSTANCE;
    }
}
