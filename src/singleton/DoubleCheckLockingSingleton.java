package singleton;

import java.util.concurrent.atomic.AtomicReference;

public class DoubleCheckLockingSingleton {
    private static final AtomicReference<DoubleCheckLockingSingleton> atomicReference = new AtomicReference<>();
    private static volatile DoubleCheckLockingSingleton INSTANCE;

    private DoubleCheckLockingSingleton() {
        System.out.println("Double Check Locking Singleton");
    }

    //    public static DoubleCheckLockingSingleton getInstance() {
//        if (INSTANCE == null) {
//            synchronized (DoubleCheckLockingSingleton.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = new DoubleCheckLockingSingleton();
//                }
//            }
//        }
//
//        return INSTANCE;
//    }

    public static DoubleCheckLockingSingleton getInstance() {
        if (atomicReference.get() == null) {
            atomicReference.set(new DoubleCheckLockingSingleton());
        }

        return atomicReference.get();
    }
}
