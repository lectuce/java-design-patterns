package singleton;

public class SingletonDemo {
    public static void main(String[] args) {
        new Thread(LazySingleton::getInstance).start();
        new Thread(LazySingleton::getInstance).start();

        new Thread(EagerSingleton::getInstance).start();
        new Thread(EagerSingleton::getInstance).start();

        new Thread(DoubleCheckLockingSingleton::getInstance).start();
        new Thread(DoubleCheckLockingSingleton::getInstance).start();
    }
}
