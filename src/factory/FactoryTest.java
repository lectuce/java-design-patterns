package factory;

public class FactoryTest {
    public static void main(String[] args) {
        SharedClass sc1 = new SharedClass();
        SharedClass sc2 = new SharedClass();
        SharedClass sc3 = new SharedClass();
        Thread thread1 = new WorkingThread("thread1", sc1);
        Thread thread2 = new WorkingThread("thread2", sc2);
        Thread thread3 = new WorkingThread("thread3", sc3);

        thread1.start();
        System.out.println("1 started");
        thread2.start();
        System.out.println("2 started");
        thread3.start();
        System.out.println("3 started");
    }
}

class SharedClass {
    public void sharedMethod(String threadName) {
        synchronized (SharedClass.class) {
            System.out.println("---------" + threadName);
            for (int i = 1; i <= 3; i++) {
                System.out.println(threadName + "-->" + i);
            }
        }
    }
}

class WorkingThread extends Thread {
    private String threadName;
    private SharedClass sc;

    WorkingThread(String threadName, SharedClass sc) {
        this.threadName = threadName;
        this.sc = sc;
    }

    @Override
    public void run() {
        this.sc.sharedMethod(this.threadName);
    }
}
