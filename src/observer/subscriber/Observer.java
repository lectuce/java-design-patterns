package observer.subscriber;

public interface Observer {
    void update(Object newInfo);
}
