package observer.subscriber;

public class ThirdObserver implements Observer {
    @Override
    public void update(Object newInformation) {
        System.out.println("Third Observer : " + newInformation);
    }
}
