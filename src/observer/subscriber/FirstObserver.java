package observer.subscriber;

public class FirstObserver implements Observer {
    @Override
    public void update(Object newInformation) {
        System.out.println("First Observer : " + newInformation);
    }
}
