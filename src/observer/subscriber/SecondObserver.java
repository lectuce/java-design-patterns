package observer.subscriber;

public class SecondObserver implements Observer {
    @Override
    public void update(Object newInformation) {
        System.out.println("Second Observer : " + newInformation);
    }
}
