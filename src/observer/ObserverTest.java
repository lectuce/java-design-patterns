package observer;

import observer.observable.ExampleSubject;
import observer.subscriber.FirstObserver;
import observer.subscriber.SecondObserver;
import observer.subscriber.ThirdObserver;

public class ObserverTest {
    public static void main(String[] args) {
        var subject = new ExampleSubject();
        var firstObserver = new FirstObserver();
        var secondObserver = new SecondObserver();
        var thirdObserver = new ThirdObserver();

        // Register observer/subscriber
        subject.addObserver(firstObserver);
        subject.addObserver(secondObserver);

        // Change information in Subject
        subject.setInformation("Subject information changed ---1");

        // Register observer/subscriber
        subject.addObserver(thirdObserver);

        // Unsubscribe from SecondObserver
        subject.removeObserver(secondObserver);

        // Change information in Subject
        subject.setInformation("Subject information changed ---2");
    }
}
