package observer.observable;

import observer.subscriber.Observer;

import java.util.ArrayList;
import java.util.List;

public class ExampleSubject implements Subject {
    private List<Observer> observers;
    private String information;

    public ExampleSubject() {
        this.observers = new ArrayList<>();
    }

    public void setInformation(String newInformation) {
        this.information = newInformation;
        this.notifyObservers(this.information);
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
        System.out.println("Registering Observer : " + observer.getClass().getSimpleName());
    }

    @Override
    public void removeObserver(Observer observer) {
        var index = this.observers.indexOf(observer);
        if (index >= 0 ) {
            this.observers.remove(index);
            System.out.println("Removing Observer : " + observer.getClass().getSimpleName());
        }
    }

    @Override
    public void notifyObservers(Object arg) {
        this.observers.forEach(observer -> observer.update(arg));
    }
}
